import pytest

from numerous.declarative.exceptions import ItemNotAssignedError
from numerous.declarative.specifications.item_spec import ItemsSpec
from tests.test_declarative.mock_objects import TestModule


def test_check_assigned():
    class TestItems(ItemsSpec):
        side1: TestModule

    items = TestItems()

    with pytest.raises(ItemNotAssignedError):
        items.get_modules()

    items.side1 = TestModule(tag='test')

    items.get_modules()

    class TestItems2(ItemsSpec):
        side1: TestModule

    items = TestItems2()

    with pytest.raises(ItemNotAssignedError):
        items.get_modules()

    items.side1 = TestModule(tag='test')

    items.get_modules()
