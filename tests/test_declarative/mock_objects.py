from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.variables.declarative_variables import Parameter
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.scope_spec import ScopeSpec


class TestSpec(ScopeSpec):
    A = Parameter(0)


class ExtTestSpec(TestSpec):
    B = Parameter(0)


class TestItemSpec(ItemsSpec):
    pass


class TestModule(Module):
    """
    Class implementing a test module
    """

    tag: str = 'mod'

    default = TestSpec()
    items = TestItemSpec()

    def __init__(self, tag=None):
        super(TestModule, self).__init__(tag)
