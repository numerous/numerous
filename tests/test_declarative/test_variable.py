from numerous.declarative.variables.declarative_variables import DeclarativeVariable


def test_instance():
    var = DeclarativeVariable()

    i1 = var.instance({})
    i2 = var.instance({})

    assert i1 != i2
