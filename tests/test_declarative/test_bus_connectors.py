import pytest

from numerous.declarative.connectors.declarativeconnector import DeclarativeConnector
from numerous.declarative.enums import Directions
from numerous.declarative.exceptions import *
from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.scope_spec import ScopeSpec
from numerous.declarative.variables.declarative_variables import Parameter
from .mock_objects import TestModule


def test_create_connector():
    class Variables(ScopeSpec):
        var1 = Parameter(0)

    variables = Variables()

    connector = DeclarativeConnector(var=variables.var1.set_direction())
    assert connector.connected == False
    assert connector.channels["var"][1] == Directions.SET


def test_add_connection():
    class Variables(ScopeSpec):
        var1 = Parameter(0)

    variables = Variables()

    connector = DeclarativeConnector(var1=variables.var1.set_direction())

    class Variables2(ScopeSpec):
        var1 = Parameter(0)

    variables2 = Variables2()

    connector2 = DeclarativeConnector(var1=variables2.var1.get_direction())

    connector >> connector2

    with pytest.raises(AlreadyConnectedError):
        connector >> connector2


class TestModuleWConnector(Module):
    """
    Class implementing a test module
    """

    tag: str = 'mod_w_conn'

    class Variables(ScopeSpec):
        var1 = Parameter(0)

    variables = Variables()

    class Items(ItemsSpec):
        mod: TestModule

    items = Items()

    connector = DeclarativeConnector(
        var1=variables.var1.get_direction(),
        mod=items.mod.get_direction()
    )

    def __init__(self, tag=None):
        super(TestModuleWConnector, self).__init__(tag)


class TestModuleWConnector2(Module):
    """
    Class implementing a test module
    """

    tag: str = 'mod_w_conn2'

    class Variables(ScopeSpec):
        var1 = Parameter(0)

    variables = Variables()

    class Items(ItemsSpec):
        mod2: TestModule

    items = Items()

    connector = DeclarativeConnector(
        var1=variables.var1.set_direction(),
        mod=items.mod2.set_direction()
    )

    def __init__(self, tag=None):
        super(TestModuleWConnector2, self).__init__(tag)
        self.items.mod2 = TestModule("test2")


class TestModuleWConnectorInOtherModule(Module):
    class Items(ItemsSpec):
        mod_w_conn_1: TestModuleWConnector
        mod_w_conn_2: TestModuleWConnector2

    items = Items()

    items.mod_w_conn_1.connector >> items.mod_w_conn_2.connector

    def __init__(self, tag):
        super(TestModuleWConnectorInOtherModule, self).__init__(tag)
        self.items.mod_w_conn_1 = TestModuleWConnector('tmwc')
        self.items.mod_w_conn_2 = TestModuleWConnector2('tmwc2')


def test_module_with_connector():
    t = TestModuleWConnectorInOtherModule('test')
    t.finalize()
