from numerous.declarative.variables.declarative_variables import Parameter
from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.scope_spec import ScopeSpec


class TestLeafModule(Module):
    """
    Class implementing a test module
    """

    class Variables(ScopeSpec):
        a = Parameter(0)

    default = Variables()

    tag: str = 'mod'

    def __init__(self, tag=None):
        super(TestLeafModule, self).__init__(tag)


class TestBranchLevel1Module(Module):
    """
    Class implementing a test module
    """

    class Items(ItemsSpec):
        branch: TestLeafModule

    tag: str = 'mod1'

    items = Items()


class TestBranchLevel2Module(Module):
    """
    Class implementing a test module
    """

    class Items(ItemsSpec):
        branch: TestBranchLevel1Module

    tag: str = 'mod2'

    items = Items()


class TestBranchLevel3Module(Module):
    """
    Class implementing a test module
    """

    class Items(ItemsSpec):
        branch: TestBranchLevel2Module

    tag: str = 'mod3'

    items = Items()

    def __init__(self, tag=None):
        super(TestBranchLevel3Module, self).__init__(tag)


