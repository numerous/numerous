from numerous.declarative.variables.declarative_variables import Parameter
from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.scope_spec import ScopeSpec


class TestSpec(ScopeSpec):
    A = Parameter(0)


class TestItemSpec(ItemsSpec):
    ...


class TestModule(Module):
    """
    Class implementing a test module
    """
    tag: str = 'mod'

    def __init__(self, tag):
        super(TestModule, self).__init__(tag)

    # @EquationSpec(default)
    def eval(self, scope: TestSpec):
        scope.var1 = 19


def test_mod():
    TestModule(tag="mod")
