
import pytest

from numerous.declarative.connectors.declarativeconnector import DeclarativeConnector
from numerous.declarative.specifications.module import Module


def test_get_connection():
    a = Module(tag="a")
    b = Module(tag="b")


    connector1 = DeclarativeConnector(a.get_direction())
    connector2 = DeclarativeConnector(b.set_direction())


    connector1.connect(connector2)

    assert connector1.connection.side2 == connector2
