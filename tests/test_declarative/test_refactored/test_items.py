

import pytest

from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.module_spec import ModuleSpec


@pytest.fixture
def TestItems():
    class Items(ItemsSpec):

        a: Module

    return Items

def test_scope_spec(TestItems):

    items = TestItems()

    assert "a" in items.__dict__
    assert isinstance(items.a, ModuleSpec)