import pytest

from numerous.declarative.connectors.declarativeconnector import DeclarativeConnector
from numerous.declarative.exceptions import ItemNotAssignedError
from numerous.declarative.generator import generate_system
from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.scope_spec import ScopeSpec
from numerous.declarative.variables.declarative_variables import DeclarativeVariable


@pytest.fixture
def TestModule():
    class TestModule(Module):
        class Variables(ScopeSpec):
            a = DeclarativeVariable(0)

        variables = Variables()

    return TestModule


@pytest.fixture
def TestModuleMissingItem(TestModule):
    class TestModuleMissingItem(Module):
        class Items(ItemsSpec):
            mod_missing: TestModule

        items = Items()

    return TestModuleMissingItem


@pytest.fixture
def TestModuleConnection(TestModule):
    class TestModuleConnection(Module):
        class Items(ItemsSpec):
            mod_to: TestModule
            mod_from: TestModule

        items = Items()

        connector_to = DeclarativeConnector(var=items.mod_to.variables.a.get_direction())
        connector_from = DeclarativeConnector(var=items.mod_from.variables.a.set_direction())

        connector_to.connect(connector_from)

        def __init__(self, tag=None):
            super(TestModuleConnection, self).__init__(tag)

            self.items.mod_from = TestModule(tag="mod_from")
            self.items.mod_to = TestModule(tag="mod_to")

    return TestModuleConnection


def test_generator(TestModule):
    mod1 = TestModule()

    sys = generate_system("system", mod1)


def test_generator(TestModuleMissingItem):
    mod1 = TestModuleMissingItem(tag="tmi")
    with pytest.raises(ItemNotAssignedError):
        sys = generate_system("system", mod1)


def test_module_connection(TestModuleConnection):
    mod = TestModuleConnection(tag="tmc")
    sys = generate_system("system", mod)
