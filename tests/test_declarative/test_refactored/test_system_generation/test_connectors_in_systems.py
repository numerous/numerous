import pytest

from numerous.declarative.connectors.declarativeconnector import DeclarativeConnector
from numerous.declarative.equations import DeclarativeEquation
from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.variables.declarative_variables import Parameter, Constant, State
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.scope_spec import ScopeSpec


@pytest.fixture
def ThermalMass():
    class ThermalMass(Module):
        class Variables(ScopeSpec):
            T, T_dot = State(0)
            P = Parameter(0)
            C = Constant(100 * 4200)

        variables = Variables()

        @DeclarativeEquation(variables)
        def eq(self, scope: Variables):
            scope.T_dot = 0.001 * scope.P

        def __init__(self, T, tag):
            super(ThermalMass, self).__init__(tag)
            self.variables.T.value = T

    return ThermalMass


@pytest.fixture
def ThermalMassConnector(ThermalMass):
    class ThermalMassConnector(Module):
        class Items(ItemsSpec):
            tm: ThermalMass

        items = Items()

        connector = DeclarativeConnector(tm=items.tm.get_direction())

    return ThermalMassConnector


@pytest.fixture
def ThermalMassConnectionAssign(ThermalMass, ThermalMassConnector):
    class ThermalMassConnectionAssign(Module):
        class Items(ItemsSpec):
            in_tm: ThermalMass
            tm_conn: ThermalMassConnector

        items = Items()

        connector = DeclarativeConnector(tm=items.in_tm.set_direction())
        connector >> items.tm_conn.connector

        def __init__(self, tag):
            super(ThermalMassConnectionAssign, self).__init__(tag)

            self.items.in_tm = ThermalMass(tag="tm", T=20)
            self.items.tm_conn = ThermalMassConnector(tag="tmc")

    return ThermalMassConnectionAssign


def test_assign_to_connected_module(ThermalMassConnectionAssign):
    test_system = ThermalMassConnectionAssign("test")
