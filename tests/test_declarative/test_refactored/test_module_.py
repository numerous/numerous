import pytest

from numerous.declarative.connectors.declarativeconnector import DeclarativeConnector
from numerous.declarative.equations import DeclarativeEquation
from numerous.declarative.mappings import create_mappings
from numerous.declarative.variables.declarative_variables import DeclarativeVariable
from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.scope_spec import ScopeSpec


@pytest.fixture
def TestModule():
    class TestModule(Module):
        class Variables(ScopeSpec):
            a = DeclarativeVariable()

        variables = Variables()

        connector = DeclarativeConnector(a=variables.a.get_direction())

    return TestModule


def test_common_ref_space(TestModule):

    mod1 = TestModule(tag="test")

    assert mod1.connector.channels['a'][0] is mod1.variables.a

    mod2 = TestModule(tag="test2")

    assert mod1.connector.a is not mod2.connector.a
    assert mod1.variables.a is not mod2.variables.a


def test_module():
    class TestSub(Module):
        class Variables(ScopeSpec):
            b = DeclarativeVariable()

        variables = Variables()

        connector = DeclarativeConnector(b=variables.b.get_direction())

        def __init__(self):
            super(TestSub, self).__init__()

        @DeclarativeEquation(variables)
        def eq(self, scope: Variables):
            scope.b = 1


    class Test(Module):
        class Variables(ScopeSpec):
            a = DeclarativeVariable()

        variables = Variables()

        class Modules(ItemsSpec):
            mod1: TestSub
            mod2: TestSub

        modules = Modules()


        with create_mappings() as mappings:
            variables.a.add_assign_mapping(modules.mod1.variables.b)
            modules.mod1.variables.b.add_sum_mapping(modules.mod2.variables.b)

        connector = DeclarativeConnector(a=variables.a.get_direction())

        def __init__(self,tag):
            super(Test, self).__init__(tag)


    test = Test(tag="test1")

    assert test.connector.channels['a'][0] is test.variables.a

    test2 = Test(tag="test3")

    assert test.variables.a is not test2.variables.a
    assert test.modules.mod1 is not test2.modules.mod1

    assert test.modules.mod1.connector.channels['b'] is not test2.modules.mod1.connector.channels['b']

    assert test.modules.mod1.variables.b is not test.modules.mod2.variables.b

