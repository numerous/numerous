import pytest

from numerous.declarative.connectors.declarativeconnector import DeclarativeConnector
from numerous.declarative.equations import DeclarativeEquation
from numerous.declarative.variables.declarative_variables import Parameter
from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.module_spec import ModuleSpec
from numerous.declarative.specifications.scope_spec import ScopeSpec


@pytest.fixture
def TestModule():
    class TestModule(Module):
        class Items(ItemsSpec):
            a: Module

        items = Items()

        class Variables(ScopeSpec):
            var1 = Parameter(0)
            var2 = Parameter(0)

        variables = Variables()

        connector = DeclarativeConnector(
            var1=variables.var1.set_direction()
        )

        connector2 = DeclarativeConnector(
            var1=variables.var2.get_direction()
        )


        connector.connect(connector2)

        @DeclarativeEquation(variables)
        def eq(self, scope: Variables):
            scope.var1 = 1.0

    return TestModule


def test_module(TestModule):

    test_module_instance = TestModule(tag="test")

    assert isinstance(test_module_instance, Module)

    assert isinstance(test_module_instance.items.a, ModuleSpec)
    assert TestModule.connector.connection.side2 is TestModule.connector2

    assert TestModule.variables != test_module_instance.variables
    assert TestModule.connector != test_module_instance.connector

    assert TestModule.variables.var1 != test_module_instance.variables.var1
    assert TestModule.variables.var1 != test_module_instance.variables.var1

    assert TestModule.variables.var1 == TestModule.connector.var1

    assert test_module_instance.variables.var1 == test_module_instance.connector.var1

    assert test_module_instance.variables.var1 is test_module_instance.connector.var1
