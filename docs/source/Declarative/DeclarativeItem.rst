**Declarative Systems**

ItemsSpec

ScopeSpec  Parameter
    - **ItemsSpec**: This is a special class where all the `Items` or `Modules` that the current
`Module` is using are defined.

3. **Connectors**: A `Connector` is used to establish a connection between different `Items`
or `Modules`. The `DeclarativeConnector` in this case is used to connect the `ThermalReservoir` with the `ThermalMass`.

The overall system is defined using these building blocks. Using this declarative approach
allows the complex system to be broken down into manageable pieces. Each `Item` or `Module`
can be individually tested and then combined to create the overall system. This can be particularly
useful for systems engineering and modelling of complex physical systems.

It's also worth noting that the code is using `pytest.fixture` decorators, which are a feature of
the pytest testing framework. Fixtures are a way of setting up and tearing down code for tests.
In this case, they're being used to define components of the thermal system that can be reused across different tests.

The `generate_system` method is a part of the Numerous framework which can be used to generate
a system based on the defined `Module`. This process includes generating all the equations and variables of the system.