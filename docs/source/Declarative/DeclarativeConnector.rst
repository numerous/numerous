Declarative Connector

The declarative way of creating mappings offers a more intuitive and high-level way of connecting variables
from different scopes. These mappings are established in a manner that maintains consistency
and modularity in the model definition.
Creating Declarative Mappings

Here's an example of creating a declarative mapping between two scopes:

python

import pytest
from numerous.declarative.specifications.scope_spec import ScopeSpec
from numerous.declarative.variables.declarative_variables import Parameter

@pytest.fixture
def TestScope():
    class TestScope(ScopeSpec):
        a = Parameter(0)
    return TestScope

def test_mapping(TestScope):
    test_scope = TestScope()
    test_scope_2 = TestScope()
    test_scope_2.a.add_assign_mapping(test_scope.a)
    assert test_scope is not test_scope_2
    assert test_scope.a is not test_scope_2.a
    assert len(test_scope_2.a.mappings) == 1
    assert test_scope.a is test_scope_2.a.mappings[0][1]

In the example above, TestScope is a scope specification with a single parameter a.
Two instances of this scope specification are created, test_scope and test_scope_2.
A mapping is then added from test_scope.a to test_scope_2.a using the add_assign_mapping function.

This function adds a mapping that will replace the value of test_scope_2.a
with the value of test_scope.a at each time step during the simulation.

Assertions in the test ensure that the two scope instances are different
, their a parameters are distinct, and that the mapping from test_scope.a to test_scope_2.a has been correctly set up.
Sum Mappings in Declarative Style


In addition to direct assignment mappings, the declarative style also supports sum mappings.
A sum mapping adds the source value to the target variable at each time step during the simulation.

Here is an example of a sum mapping:

python

def test_mapping_sum(TestScope):
    test_scope = TestScope()
    test_scope_2 = TestScope()
    test_scope_2.a.add_assign_mapping(test_scope.a)
    assert len(test_scope_2.a.mappings) == 1
    assert test_scope.a is test_scope_2.a.mappings[0][1]

In this test, the add_assign_mapping function is used again to create a mapping
from test_scope.a to test_scope_2.a. The result is that the value of test_scope.a is added
to test_scope_2.a at each time step during the simulation.

Like before, assertions in the test confirm the correct setup of the mapping.
Conclusion

In conclusion, the declarative way of creating mappings provides a high-level,
intuitive approach for connecting variables in a system model.
This style enhances code readability and eases the process of model definition.