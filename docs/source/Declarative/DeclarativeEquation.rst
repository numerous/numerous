#   DeclarativeEquation

The `DeclarativeEquation` is a part of the numerous package which provides a
 different from to define  the standard `Equation` in that it allows you to specify equations using a more intuitive,
declarative style.

Declarative equations are written as methods within a class,
but unlike standard equations, they use a decorator `@DeclarativeEquation`.
This decorator takes in the variables scope of the class and manages the variables'
relationships and changes over time.
It allows a more intuitive access to declared variables
and provides a clear way to express the relationships between the variables.

In the given example, a `Conductor` class is defined as a `Module`.
This class contains a `Variables` sub-class that defines the variables of the system,
and an `Items` sub-class that specifies the different components or items that make up the system.

## Usage

A `DeclarativeEquation` is used inside a `Module` as a method
and decorated using the `@DeclarativeEquation(variables)` decorator.
The `variables` in the decorator represent the class's variables scope.
An example usage of `DeclarativeEquation` is shown in the `diff` method of the `Conductor` class in the provided code:

```python
@DeclarativeEquation(variables)
def diff(self, scope: Variables):
    P = (scope.side1_T - scope.side2_T) * 100
    scope.side1_P = -P
    scope.side2_P = P
```

Here, `diff` method defines a `DeclarativeEquation` where the pressure difference (`P`)
between two sides is calculated and assigned.

## Difference from Equation

While both `DeclarativeEquation` and `Equation` are used to define system equations,
they are used in different contexts and have a few key differences:

- **Usage Context**: `DeclarativeEquation` is used within a `Module`, while `Equation` is
typically used within a class that inherits `EquationBase`.

- **Declarative vs Procedural**: As the name suggests, `DeclarativeEquation` is a
declarative way of defining relationships between variables, making it easy to express complex
relationships in a more straightforward way.
On the other hand, `Equation` is more procedural, requiring explicit definition of each equation.

- **Variable Scopes**: `DeclarativeEquation` utilizes a different way of managing variables' scope.
Instead of using `add_state()`, `add_parameter()`, etc., methods to define variables in the equation's
namespace, `DeclarativeEquation` declares a `Variables` sub-class to define the variables, providing
a cleaner way to organize the variables.

- **Item Mapping**: `DeclarativeEquation` provides a simpler way to map items' variables,
as seen in the `Conductor` class where the `side1` and `side2` items' variables are mapped
directly to the `Variables` class variables.

In summary, while `Equation` and `DeclarativeEquation` both allow defining system equations,
they are suited for different scenarios and provide different levels of abstraction.
The choice between them would depend on the specific requirements and complexity of the system being modeled.