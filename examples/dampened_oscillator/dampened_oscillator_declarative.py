import numpy as np

from numerous.declarative.connectors.declarativeconnector import DeclarativeConnector
from numerous.declarative.connectors.module_connections import create_connections
from numerous.declarative.generator import generate_system
from numerous.declarative.specifications.equation_spec import EquationSpec
from numerous.declarative.specifications.item_spec import ItemsSpec
from numerous.declarative.specifications.module import Module
from numerous.declarative.specifications.scope_spec import ScopeSpec
from numerous.declarative.variables.declarative_variables import Constant, Parameter, State
from numerous.engine.system import Connector


class DampenedOscillator(Module):
    """
        Equation and item modelling a spring and dampener
    """

    class Mechanics(ScopeSpec):
        # Define variables
        k = Constant(1)  # spring constant
        c = Constant(1)  # coefficient of friction
        a = Parameter(0)  # acceleration
        x, x_dot = State(0)  # distance
        v, v_dot = State(0)  # velocity

    mechanics = Mechanics()

    coupling = DeclarativeConnector(
        x=mechanics.x.set_direction(),
        F=mechanics.v_dot.get_direction()
    )

    def __init__(self, tag="dampened_oscillator"):
        super().__init__(tag)
        self.tag = tag

    @EquationSpec(mechanics)
    def eval(self, scope: Mechanics):
        scope.a = -scope.k * scope.x - scope.c * scope.v
        scope.v_dot = scope.a
        scope.x_dot = scope.v


class SpringCoupling(Module):
    tag = "springcoup"

    class Mechanics(ScopeSpec):
        k = Parameter(1)
        c = Parameter(1)
        F1 = Parameter(0)
        F2 = Parameter(0)
        x1 = Parameter(0)
        x2 = Parameter(0)

    mechanics = Mechanics()

    side1 = DeclarativeConnector(
        F=mechanics.F1.set_direction(),
        x=mechanics.x1.get_direction()
    )

    side2 = DeclarativeConnector(
        F=mechanics.F2.set_direction(),
        x=mechanics.x2.get_direction()
    )

    def __init__(self, tag, k=1.0):
        super().__init__(tag)

        self.mechanics.set_values(k=k)

    @EquationSpec(mechanics)
    def eval(self, scope: Mechanics):
        scope.c = scope.k

        dx = scope.x1 - scope.x2
        F = np.abs(dx) * scope.c

        scope.F1 = -F if scope.x1 > scope.x2 else F  # [kg/s]

        scope.F2 = -scope.F1


class OscillatorSystem(Module):
    class Items(ItemsSpec):
        # The oscillator system will have two oscillators interacting via the coupling
        oscillator1: DampenedOscillator
        oscillator2: DampenedOscillator
        coupling: SpringCoupling

    items = Items()

    with create_connections() as _:
        # connect oscillators via the coupling
        items.oscillator1.coupling >> items.coupling.side1
        items.oscillator2.coupling >> items.coupling.side2

    def __init__(self, k=0.01, c=0.001, x0=(1, 2), tag=None):
        super(OscillatorSystem, self).__init__(tag=tag)

        # Initialized the modules in the oscillator system

        self.items.oscillator1 = DampenedOscillator(tag="dampened_oscillator1")
        self.items.oscillator1.mechanics.set_values(k=k, c=c, x=x0[0])

        self.items.oscillator2 = DampenedOscillator(tag="dampened_oscillator2")
        self.items.oscillator2.mechanics.set_values(k=k, c=c, x=x0[1])

        self.oscillator3 = DampenedOscillator(tag="dampened_oscillator3")

        self.items.oscillator2.mechanics.v_dot += self.oscillator3.mechanics.a
        self.oscillator3.mechanics.v_dot = self.items.oscillator1.mechanics.a

        self.items.coupling = SpringCoupling(k=k, tag="spring_coupling")


if __name__ == "__main__":
    from numerous.engine import simulation
    from numerous.engine import model
    from matplotlib import pyplot as plt
    import logging

    logging.basicConfig(level=logging.DEBUG)

    oscillator_system = OscillatorSystem(k=0.01, c=0.001, x0=[1.0, 2.0], tag="oscillator_system")

    system = generate_system('system', oscillator_system)

    # Define simulation
    s = simulation.Simulation(model.Model(system, use_llvm=False), t_start=0, t_stop=500.0, num=1000, num_inner=100,
                              max_step=1)

    # Solve and plot
    s.solve()

    s.model.historian_df[['system.oscillator1.mechanics.x', 'system.oscillator2.mechanics.x']].plot()
    plt.show()
    plt.interactive(False)
